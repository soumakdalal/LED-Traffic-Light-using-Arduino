# LED-Traffic-Light-using-Arduino

#Required Hardware
* 1x Arduino UNO
* 1x A-B USB Cable
* 1x Red LED
* 1x Yellow LED
* 1x Green LED
* 9x Male to Male Jumper Wires
* 1x Breadboard

#Required Software
* Arduino IDE
