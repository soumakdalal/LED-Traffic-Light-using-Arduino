/*
LED Traffic Light using Arduino

LED Traffic Light Source Code
created 5 July 2016
by Soumak Dalal

*/

/////////////////////////
//Adjust the pin below//
///////////////////////

int green = 11;
int amber = 12;
int red = 13;

//////////////////////////////
//Adjust the variables below/
////////////////////////////

void setup(){
pinMode (green,OUTPUT);
pinMode(amber,OUTPUT);
pinMode(red,OUTPUT);
pinMode(amber,OUTPUT);
pinMode (green,OUTPUT);
}

//////////////////////////////
//Adjust the variables below/
////////////////////////////

void loop()
{
digitalWrite(green,HIGH);
delay(60000);
digitalWrite(green,LOW);
digitalWrite(amber,HIGH);
delay(5000);
digitalWrite(amber,LOW);
digitalWrite(red,HIGH);
delay(30000);
digitalWrite(red,LOW);
digitalWrite(amber,HIGH);
delay(5000);
digitalWrite(amber,LOW);
digitalWrite(green,HIGH);
delay(60000);
digitalWrite(green,LOW);
}
